<!doctype html>
<html lang="en">

<head>
    <title>Title</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css">
    <link rel="stylesheet" type="text/css"href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">

</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <h1 class="text-success mt-4">Student Validation Update Form</h1>
            </div>
        </div>
        <form action="{{ '/update' }}/{{ $data->id }}" method="POST" id="myform">
            @csrf
            <div class="row mt-3">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="">Name</label>
                        <input type="text" class="form-control" name="name" id="" value="{{ $data->name }}"
                            aria-describedby="helpId" placeholder="Enter Your Name" required>

                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="">Father Name</label>
                        <input type="text" class="form-control" name="fname" id="" value="{{ $data->fname }}"
                            aria-describedby="helpId" placeholder="Enter Your Father Name">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="">Email</label>
                        <input type="email" class="form-control" name="email" id="" value="{{ $data->email }}"
                            aria-describedby="helpId" placeholder="Enter Your Email">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="">Address</label>
                        <input type="text" class="form-control" name="address" id="" value="{{ $data->address }}"
                            aria-describedby="helpId" placeholder="Enter Your Address">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="">Date Of Birth</label>
                        <input type="date" class="form-control" name="dob" id="" value="{{ $data->dob }}"
                            aria-describedby="helpId" placeholder="">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="">Phone Number</label>
                        <input type="number" class="form-control" name="phone" id="" value="{{ $data->phone }}"
                            aria-describedby="helpId" placeholder="Enter Your Number">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Description</label>
                        <textarea class="form-control" name="des" id="" rows="3" value="{{ $data->des }}">{{ $data->des }}</textarea>
                    </div>
                </div>
            </div>
            <input type="hidden" name="sid" value="{{ $data->id }}">
            <div class="row">
                <div class="col-md-12 mt-2">
                    <button class="btn btn-success" id="btn" role="button">Update</button>
                </div>
            </div>
        </form>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>
    <script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

</body>
</html>

<script>
    $('#btn').on('click', function(e) {
        e.preventDefault();
        let data = new FormData(myform)
        $.ajax({
            url: "http://127.0.0.1:8000/api/update",
            method: "POST",
            data: data,
            contentType: false,
            processData: false,
            success: function(res) {
                if (res.update) {
                    window.location.href="http://127.0.0.1:8000/table"
                    toastr.success(res.update, 'Data!', {timeOut: 2000})
                }
            },
        });
    });
</script>

<script>
    toastr.options = {
        "closeButton": true,
        "progressBar": true
    }
</script>
