<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Form;

class FormController extends Controller
{
    public function show(){
        return view('form');
    }

    public function tabledata(){
        return view('table');
    }

    public function edit($id){
        $data = Form::where('id',$id)->first();
        return view('update')->with(compact('data'));
    }

}
