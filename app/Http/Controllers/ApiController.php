<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Form;

class ApiController extends Controller
{
    public function add(Request $request){
        $request->validate([
            "name" => 'required',
            "fname" => 'required',
            "email" => 'required|email',
            "address" => 'required',
            "dob" => 'required',
            "phone" => 'required',
            "des" => 'required',
        ]);
        $add = Form::create([
            'name' => $request->name,
            'fname' => $request->fname,
            'email' => $request->email,
            'address' => $request->address,
            'dob' => $request->dob,
            'phone' => $request->phone,
            'des' => $request->des,
        ]);
        if ($add) {
            return response()->json([
                'status' => 200,
                'done' => 'Student Has Been Inserted'
            ]);
        }
    }

    public function show(){
        $data = Form::all();
        return response()->json([
            'status' => 200,
            'data' => $data
        ]);
    }

    public function delete($id){
        $delete = Form::where('id',$id)->delete();
        if ($delete) {
            return response()->json([
                'status' => 200,
                'delete' => 'Student Has Been Deleted'
            ]);
        }
    }

    public function update(Request $request){
        $update = Form::where('id',$request->sid)->update([
            'name' => $request->name,
            'fname' => $request->fname,
            'email' => $request->email,
            'address' => $request->address,
            'dob' => $request->dob,
            'phone' => $request->phone,
            'des' => $request->des,
        ]);
        if ($update) {
            return response()->json([
                'status' => 200,
                'update' => 'Student Has Been Updated'
            ]);
        }
    }

}
