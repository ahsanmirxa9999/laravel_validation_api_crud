<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ApiController;

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('add',[ApiController::class,'add']);
Route::get('show',[ApiController::class,'show']);
Route::get('/delete/{id}',[ApiController::class,'delete']);
Route::post('/update',[ApiController::class,'update']);
