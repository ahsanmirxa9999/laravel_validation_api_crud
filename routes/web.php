<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\FormController;

// Route::get('/', function () {
//     return view('form');
// });

Route::get('/',[FormController::class,'show']);
Route::get('/table',[FormController::class,'tabledata']);
Route::get('edit/{id}',[FormController::class,'edit']);
